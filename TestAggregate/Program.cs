﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAggregate
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Func<Action, Action>> list = new List<Func<Action, Action>>();
            Func<Action, Action> first =
                next =>
                () =>
                {
                    Console.WriteLine("first");
                    next();
                };

            Func<Action, Action> second =
              next =>
              () =>
              {
                  Console.WriteLine("second");
                  next();
              };


            list.Add(first);
            list.Add(second);
            Action seed = () => { Console.WriteLine("seed"); };

            list.Reverse();
            Action action = list.Aggregate(seed, (next, current) => current(next));
            action();
        }
    }
}
